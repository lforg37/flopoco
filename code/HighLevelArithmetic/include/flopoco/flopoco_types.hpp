#ifndef FLOPOCO_TYPES_HPP
#define FLOPOCO_TYPES_HPP

#include <cstdint>

namespace flopoco
{
	using bitweight_t = std::int32_t;
	using vecwidth_t = std::uint32_t;
} // namespace flopoco
#endif