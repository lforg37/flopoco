add_flopocolib_src(
	FixFIR.cpp
	FixHalfSine.cpp
	FixIIR.cpp
	FixIIRShiftAdd.cpp
	FixRootRaisedCosine.cpp
	FixSOPC.cpp
)
